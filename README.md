# Tutorial 3 Practice

We will work in this Git repo for today's tutorial (Tutorial # 3) (25.10.2022)

To start working in this repo, perform the following steps:

1. **Clone the git repo** to your local machine: `git clone <link from repo>`, where `<link from repo>` is copied from the Gitlab repo

2. After the repo has been successfully cloned on your local machine, **change the folder** into the directory of the repo (Hint: Use the `cd` command followed by the folder name)

3. **Create your own branch** - to create your own branch, use `git checkout -b <username>` where `<username>` could be your ZEDAT ID (this could be anything, but we will keep it to ZEDAT ID to help tracking)

4. **Check which branch** you are on using `git status`

5. Now you can **start writing** your own code and make commits to the repo!
